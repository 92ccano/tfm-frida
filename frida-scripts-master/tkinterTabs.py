#!/usr/bin/env python 
from Tkinter import * 
import Tkinter as tki
from ScrolledText import ScrolledText
import ttk
import frida
import sys
import os 
import time
from optparse import OptionParser
from threading import Thread
import threading

main = Tk()
main.title('FriOS tool')
main.geometry("1000x500+100+100")
path = "hooks"
rows = 0
labels = {}
storageLabelsState = {}
connectivityLabelState = {}
jailbreakLabelState = {}
pinningLabelState = {}
checkBoxes = {}
checkBoxesVar = {}
storageLabels = {}
connectivityLabels = {}
pinningLabels = {}
jailbreakLabels = {}
wholeMessage = ""
lastClipboardValue = ""



def on_message(message, data):
	global wholeMessage
	try:
		if message:
			if(wholeMessage == ""):
				wholeMessage = message["payload"]
			else:
				wholeMessage = message["payload"]
				parseAndWriteInCorrespondingTab(wholeMessage)
						
	except Exception as e:
		writeInMainScrollText(message)
		writeInMainScrollText(e)

def onImportMessage(message,data):
	manager = importManager(message)
	thread = threading.Thread(target=manager.run)
	thread.start()

def onLastClipboardMessage(message, data):
	splitedClipboardMessage = message["payload"].split('-')
	global lastClipboardValue
	if(lastClipboardValue == ""):
		lastClipboardValue = splitedClipboardMessage[1]
		updateLastClipboardValue(1,splitedClipboardMessage[1])
	elif(lastClipboardValue != splitedClipboardMessage[1]):
		lastClipboardValue = splitedClipboardMessage[1]
		updateLastClipboardValue(2,splitedClipboardMessage[1])


class importManager(Thread):
	def __init__(self, message):
		self.message = message
	def run(self):
		if("NSUserDefaults" in self.message["payload"]):
			foundStorageLabel("NSUserDefaults")
		elif("libsqlite3.dylib" in self.message["payload"] or "sqlite3" in self.message["payload"]):
			foundStorageLabel("Sqlite3")
		elif("CoreData" in self.message["payload"]):
			foundStorageLabel("CoreData")
		elif("libc++.dylib" in self.message["payload"]):
			foundStorageLabel("RealmIO")
		del(self)
		

class mainWriter(Thread):
	global scrolledText
	def __init__(self, message):
		self.message = message
	def run(self):
		scrolledText.configure(state=tki.NORMAL)
		scrolledText.insert(END, self.message + "\n")
		scrolledText.configure(state=tki.DISABLED)
		del(self)

class storageWriter(Thread):
	global scrolledTextStorage
	def __init__(self, message):
		self.message = message
	def run(self):

		scrolledTextStorage.configure(state=tki.NORMAL)
		scrolledTextStorage.insert(END, self.message + "\n")
		scrolledTextStorage.configure(state=tki.DISABLED)
		del(self)

class connectivityWriter(Thread):
	global scrolledTextConnectivity
	def __init__(self, message):
		self.message = message
	def run(self):
		scrolledTextConnectivity.configure(state=tki.NORMAL)
		scrolledTextConnectivity.insert(END, self.message + "\n")
		scrolledTextConnectivity.configure(state=tki.DISABLED)
		del(self)

class jailbreakWriter(Thread):
	global scrolledTextConnectivity
	def __init__(self, message):
		self.message = message
	def run(self):
		scrolledTextJailbreak.configure(state=tki.NORMAL)
		scrolledTextJailbreak.insert(END, self.message + "\n")
		scrolledTextJailbreak.configure(state=tki.DISABLED)
		del(self)

class pinningWriter(Thread):
	global scrolledTextConnectivity
	def __init__(self, message):
		self.message = message
	def run(self):
		scrolledTextPinning.configure(state=tki.NORMAL)
		scrolledTextPinning.insert(END, self.message + "\n")
		scrolledTextPinning.configure(state=tki.DISABLED)
		del(self)

class openContinuouslyTheScript(Thread):
	global scrolledTextConnectivity
	def __init__(self, script, session):
		self.script = script
		self.session = session
	def run(self):
		while(True):
			hook = open(path+os.sep+self.script, "r")
			script = self.session.create_script(hook.read())
			script.on('message', onLastClipboardMessage)
			script.load()
			time.sleep(1)

class changeLastClipboardValue(Thread):
	global scrolledText, scrolledTextStorage
	def __init__(self, number, value):
		self.number = number
		self.value = value
	def run(self):
		print(self.value)
		print(lastClipboardValue)
		storageLabelsState[5]["text"] = self.value
		if(self.number == 2):
			scrolledText.configure(state=tki.NORMAL)
			scrolledText.insert(END, "Last clipboard item has changed, now is: " + self.value + "\n")
			scrolledText.configure(state=tki.DISABLED)
			scrolledTextStorage.configure(state=tki.NORMAL)
			scrolledTextStorage.insert(END, "Last clipboard item has changed, now is: " + self.value + "\n")
			scrolledTextStorage.configure(state=tki.DISABLED)

def writeInMainScrollText(message):
	manager = mainWriter(message)
	thread = threading.Thread(target=manager.run)
	thread.start()

def writeInStorageTab(message):
	manager = storageWriter(message)
	thread = threading.Thread(target=manager.run)
	thread.start()

def writeInConnectivityTab(message):
	manager = connectivityWriter(message)
	thread = threading.Thread(target=manager.run)
	thread.start()

def writeInJailbreakTab(message):
	manager = jailbreakWriter(message)
	thread = threading.Thread(target=manager.run)
	thread.start()

def writeInPinningTab(message):
	manager = pinningWriter(message)
	thread = threading.Thread(target=manager.run)
	thread.start()

def updateLastClipboardValue(number, value):
	manager = changeLastClipboardValue(number, value)
	thread = threading.Thread(target=manager.run)
	thread.start()



def foundStorageLabel(message):
	for i in storageLabels:
		if (message.lower() in storageLabels[i]["text"].lower() ):
			storageLabelsState[i]["text"] = "Found"

def enableStorageLabelStateDependingOnMessage(message):
	for i in storageLabels:
		if (message.lower() in storageLabels[i]["text"].lower() ):
			storageLabelsState[i]["text"] = "USED"

def enableConnectivityLabelDependingOnMessage(message):
	for i in connectivityLabels:
		if (message.lower() in connectivityLabels[i]["text"].lower() ):
			connectivityLabelsState[i]["text"] = "USED"

def enableJailbreakLabelDependingOnMessage(message):
	for i in jailbreakLabels:
		if (message.lower() in jailbreakLabels[i]["text"].lower() ):
			jailbreakLabelsState[i]["text"] = "USED"

def enablePinningLabelDependingOnMessage(message):
	for i in jailbreakLabels:
		if (message.lower() in jailbreakLabels[i]["text"].lower() ):
			jailbreakLabelsState[i]["text"] = "USED"

def parseAndWriteInCorrespondingTab(message):
	writeInMainScrollText(message)	
	splittedMessage = message.split('-')
	detailedMessage = ""
	if(len(splittedMessage) > 1):
		for i in range(1, len(splittedMessage)):
			detailedMessage = detailedMessage + splittedMessage[i] + "-"
		detailedMessage = detailedMessage[:-1]
	if(splittedMessage[0] == "Storage" or splittedMessage[0] == "storage"):
		if ("has been used" in detailedMessage):
			enableStorageLabelStateDependingOnMessage(splittedMessage[1])
		else:
			writeInStorageTab(detailedMessage)
	elif(splittedMessage[0] == "Connectivity" or splittedMessage[0] == "connectivity"):
		if ("has been used" in detailedMessage):
			enableConnectivityLabelDependingOnMessage(splittedMessage[1])
		else:
			writeInConnectivityTab(detailedMessage)
	elif(splittedMessage[0] == "Jailbreak" or splittedMessage[0] == "jailbreak"):
		if ("has been used" in detailedMessage):
			enableJailbreakLabelDependingOnMessage(splittedMessage[1])
		else:
			writeInJailbreakTab(detailedMessage)
	elif(splittedMessage[0] == "Pinning" or splittedMessage[0] == "pinning"):
		if ("has been used" in detailedMessage):
			enablePinningLabelDependingOnMessage(splittedMessage[1])
		else:
			writeInPinningTab(detailedMessage)
	 

	

def eraseScrolledTexts():
	scrolledText.configure(state = tki.NORMAL)
	scrolledText.delete(0.0,END)
	scrolledText.configure(state = tki.DISABLED)

def myfunction(event):
	canvas.configure(scrollregion=canvas.bbox("all"),width=300,height=435)

def myfunctionStorage(event):
	canvasStorage.configure(scrollregion=canvasStorage.bbox("all"), width = 300, height = 435)

def myfunctionConnectivity(event):
	canvasConnectivity.configure(scrollregion=canvasConnectivity.bbox("all"), width = 300, height = 435)

def myfunctionJailbreak(event):
	canvasJailbreak.configure(scrollregion=canvasJailbreak.bbox("all"),width = 300, height = 435)

def myfunctionPinning(event):
	canvasPinning.configure(scrollregion=canvasPinning.bbox("all"),width = 300, height = 435)

def runSelectedScript(session,script):
	writeInMainScrollText("[*] Parsing hook: " + script)
	if ("ContinuousGetLastClipboard" in script):
		manager = openContinuouslyTheScript(script, session)
		thread = threading.Thread(target=manager.run)
		thread.start()
		
	else:
		hook = open(path+os.sep+script, "r")
		script = session.create_script(hook.read())
		script.on('message', on_message)
		script.load()

def runImportsScript(session):
	hook = open("recognizeImports","r")
	script = session.create_script(hook.read())
	script.on('message', onImportMessage)
	script.load()


def startCallback():
	selectedScripts = {}
	numberOfScriptsToAttach = 0;
	eraseScrolledTexts()

	writeInMainScrollText("[*] Attaching to " + "Gadget" + "\n")
	try:
		session = frida.get_remote_device().attach(str("Gadget"))
		runImportsScript(session)
	except Exception as e:
		writeInMainScrollText(str(e))
	for count in checkBoxesVar:
		if(checkBoxesVar[count].get() == 1):
			selectedScripts[numberOfScriptsToAttach] = labels[count]["text"]
			numberOfScriptsToAttach = numberOfScriptsToAttach + 1
	if (len(selectedScripts) == 0):
		writeInMainScrollText("No scripts have been selected ")
	
	for count in selectedScripts:
		runSelectedScript(session,selectedScripts[count])

	#Delete all selected scripts to force the recheck next time
	del(selectedScripts)

#Main
nb = ttk.Notebook(main)
nb.pack(fill = BOTH, expand = True)
 
mainPage = ttk.Frame(nb)
nb.add(mainPage, text = "General")

attachButton = ttk.Button(mainPage, text = "Attach & Start!", command = startCallback)
attachButton.pack(fill=X)



#Left part in main
myframe=Frame(mainPage,width=300,height=435)
myframe.pack(side = LEFT)
canvas=Canvas(myframe)
frame=Frame(canvas)
myscrollbar=Scrollbar(myframe,orient="vertical",command=canvas.yview)
canvas.configure(yscrollcommand=myscrollbar.set)
myscrollbar.pack(side=RIGHT,fill=Y)
canvas.pack(side=LEFT)
canvas.create_window(200,200,window = frame,anchor="w")
frame.bind("<Configure>",myfunction)


scrolledText = ScrolledText(mainPage, undo = False, state = tki.DISABLED)
scrolledText['font'] = ('consolas','12')
scrolledText.pack(side=RIGHT,fill=Y)


auxCol = 0
auxRow = 4
count = 0


for filename in os.listdir(path):
	name, ext = os.path.splitext(filename)
	labels[count] = ttk.Label(frame, text = name + ext)
	labels[count].grid(column = auxCol, row = auxRow, sticky = W)
	checkBoxesVar[count] = IntVar()
	checkBoxes[count] = ttk.Checkbutton(frame, variable = checkBoxesVar[count])
	checkBoxes[count].grid(column = auxCol + 20 , row = auxRow, sticky = W)
	auxRow = auxRow + 2
	count = count + 1


#Storage
storageTab = ttk.Frame(nb)
nb.add(storageTab, text = "Storage")

myframeStorage=Frame(storageTab,width=300,height=435)
myframeStorage.pack(side = LEFT)
canvasStorage=Canvas(myframeStorage)
frameStorage=Frame(canvasStorage)
myscrollbarStorage=Scrollbar(myframeStorage,orient="vertical",command=canvasStorage.yview)
canvasStorage.configure(yscrollcommand=myscrollbarStorage.set)
myscrollbarStorage.pack(side=RIGHT,fill=Y)
canvasStorage.pack(side=LEFT,fill = Y)
canvasStorage.create_window(0,0,window = frameStorage,anchor="w")
frameStorage.bind("<Configure>",myfunctionStorage)

scrolledTextStorage = ScrolledText(storageTab, undo = False, state = tki.DISABLED)
scrolledTextStorage['font'] = ('consolas','12')
scrolledTextStorage.pack(side = RIGHT,fill = Y)

storageLabels[0] = ttk.Label(frameStorage,text = "NSUserDefaults: ")
storageLabels[0].grid(column = 2, row = 0, sticky = W)
storageLabelsState[0] = ttk.Label(frameStorage,text = "Found")
storageLabelsState[0].grid(column = 3, row = 0, sticky = W)
storageLabels[1] = ttk.Label(frameStorage,text = "CoreData: ")
storageLabels[1].grid(column = 2,row = 2, sticky = W)
storageLabelsState[1] = ttk.Label(frameStorage,text = "Not Found")
storageLabelsState[1].grid(column = 3, row = 2, sticky = W)
storageLabels[2] = ttk.Label(frameStorage,text = "RealmIO: ")
storageLabels[2].grid(column = 2, row = 4, sticky = W)
storageLabelsState[2] = ttk.Label(frameStorage,text = "Not Found")
storageLabelsState[2].grid(column = 3, row = 4, sticky = W)
storageLabels[3] = ttk.Label(frameStorage, text = "SQLite3: ")
storageLabels[3].grid(column = 2, row = 6, sticky = W)
storageLabelsState[3] = ttk.Label(frameStorage,text = "Not Found")
storageLabelsState[3].grid(column = 3, row = 6, sticky = W)
storageLabels[4] = ttk.Label(frameStorage, text = "NSFileManager: ")
storageLabels[4].grid(column = 2, row = 8, sticky = W)
storageLabelsState[4] = ttk.Label(frameStorage,text = "Found")
storageLabelsState[4].grid(column = 3, row = 8, sticky = W)
storageLabels[5] = ttk.Label(frameStorage, text = "Clipboard last item: ")
storageLabels[5].grid(column = 2, row = 10, sticky = W)
storageLabelsState[5] = ttk.Label(frameStorage,text = "Not Found")
storageLabelsState[5].grid(column = 3, row = 10, sticky = W)


#Connectivity
connectivityTab = ttk.Frame(nb)
nb.add(connectivityTab, text = "Connectivity")

myframeConnectivity=Frame(connectivityTab,width=300,height=435)
myframeConnectivity.pack(side = LEFT)
canvasConnectivity=Canvas(myframeConnectivity)
frameConnectivity=Frame(canvasConnectivity)
myscrollbarConnectivity=Scrollbar(myframeConnectivity,orient="vertical",command=canvasConnectivity.yview)
canvasConnectivity.configure(yscrollcommand=myscrollbarConnectivity.set)
myscrollbarConnectivity.pack(side=RIGHT,fill=Y)
canvasConnectivity.pack(side=LEFT,fill = Y)
canvasConnectivity.create_window(0,0,window = frameConnectivity,anchor="w")
frameConnectivity.bind("<Configure>",myfunctionConnectivity)

scrolledTextConnectivity = ScrolledText(connectivityTab, undo = False, state = tki.DISABLED)
scrolledTextConnectivity['font'] = ('consolas','12')
scrolledTextConnectivity.pack(side = RIGHT,fill = Y)

connectivityLabels[0] =  ttk.Label(frameConnectivity, text = "URL Instantiate: ")
connectivityLabels[0].grid(column = 2, row = 0, sticky = W)
connectivityLabelState[0] = ttk.Label(frameConnectivity,text = "Not Found")
connectivityLabelState[0].grid(column = 3, row = 0, sticky = W)
connectivityLabels[1] = ttk.Label(frameConnectivity, text = "NSHost: ")
connectivityLabels[1].grid(column = 2, row = 2, sticky = W)
connectivityLabelState[1] = ttk.Label(frameConnectivity,text = "Not Found")
connectivityLabelState[1].grid(column = 3, row = 2, sticky = W)

#Jailbreak Detection
jailbreakTab = ttk.Frame(nb)
nb.add(jailbreakTab, text = "Jailbreak Detection")

myFrameJailbreak=Frame(jailbreakTab,width=300,height=435)
myFrameJailbreak.pack(side = LEFT)
canvasJailbreak=Canvas(myFrameJailbreak)
frameJailbreak=Frame(canvasJailbreak)
myscrollbarJailbreak=Scrollbar(myFrameJailbreak,orient="vertical",command=canvasJailbreak.yview)
canvasJailbreak.configure(yscrollcommand=myscrollbarJailbreak.set)
myscrollbarJailbreak.pack(side=RIGHT,fill=Y)
canvasJailbreak.pack(side=LEFT,fill = Y)
canvasJailbreak.create_window(0,0,window = frameJailbreak,anchor="w")
frameJailbreak.bind("<Configure>",myfunctionJailbreak)

scrolledTextJailbreak = ScrolledText(jailbreakTab, undo = False, state = tki.DISABLED)
scrolledTextJailbreak['font'] = ('consolas','12')
scrolledTextJailbreak.pack(side = RIGHT,fill = Y)

jailbreakLabels[0] = ttk.Label(frameJailbreak, text = "Has access to jailbreak paths: ")
jailbreakLabels[0].grid(column = 2,row = 0, sticky = W)
jailbreakLabelState[0] = ttk.Label(frameJailbreak,text = "Not Found")
jailbreakLabelState[0].grid(column = 3, row = 0, sticky = W)
jailbreakLabels[1] = ttk.Label(frameJailbreak, text = "Has admin rights to forbiden paths: ")
jailbreakLabels[1].grid(column = 2, row = 2, sticky = W)
jailbreakLabelState[1] = ttk.Label(frameJailbreak,text = "Not Found")
jailbreakLabelState[1].grid(column = 3, row = 2, sticky = W)
jailbreakLabels[2] = ttk.Label(frameJailbreak, text = "Cydia URL Schema")
jailbreakLabels[2].grid(column = 2, row = 4, sticky = W)
jailbreakLabelState[3] = ttk.Label(frameJailbreak,text = "Not Found")
jailbreakLabelState[3].grid(column = 3, row = 4, sticky = W)
'''Left catching jailbreak through C functions'''

#Pinning
pinningTab = ttk.Frame(nb)
nb.add(pinningTab, text = "Pinning")


myFramePinning=Frame(pinningTab,width=300,height=435)
myFramePinning.pack(side = LEFT)
canvasPinning=Canvas(myFramePinning)
framePinning=Frame(canvasPinning)
myscrollbarPinning=Scrollbar(myFramePinning,orient="vertical",command=canvasPinning.yview)
canvasPinning.configure(yscrollcommand=myscrollbarPinning.set)
myscrollbarPinning.pack(side=RIGHT,fill=Y)
canvasPinning.pack(side=LEFT,fill = Y)
canvasPinning.create_window(0,0,window = framePinning,anchor="w")
framePinning.bind("<Configure>",myfunctionPinning)

scrolledTextPinning = ScrolledText(pinningTab, undo = False, state = tki.DISABLED)
scrolledTextPinning['font'] = ('consolas','12')
scrolledTextPinning.pack(side = RIGHT,fill = Y)

pinningLabels[0] = ttk.Label(framePinning, text = "SSL Handshake: ")
pinningLabels[0].grid(column = 2, row = 0, sticky = W)
pinningLabelState[0] = ttk.Label(framePinning,text = "Not Found")
pinningLabelState[0].grid(column = 3, row = 0, sticky = W)
pinningLabels[1] = ttk.Label(framePinning, text = "SSL Set session option: ")
pinningLabels[1].grid(column = 2, row = 2, sticky = W)
pinningLabelState[1] = ttk.Label(framePinning,text = "Not Found")
pinningLabelState[1].grid(column = 3, row = 2, sticky = W)
pinningLabels[2] = ttk.Label(framePinning, text = "SSL Create Context: ")
pinningLabels[2].grid(column = 2, row = 4, sticky = W)
pinningLabelState[2] = ttk.Label(framePinning,text = "Not Found")
pinningLabelState[2].grid(column = 3, row = 4, sticky = W)
pinningLabels[3] = ttk.Label(framePinning, text = "Chain Validation: ")
pinningLabels[3].grid(column = 2, row = 4, sticky = W)
pinningLabelState[3] = ttk.Label(framePinning,text = "Not Found")
pinningLabelState[3].grid(column = 3, row = 4, sticky = W)
pinningLabels[4] = ttk.Label(framePinning, text = "Cert Validation: ")
pinningLabels[4].grid(column = 2, row = 6, sticky = W)
pinningLabelState[4] = ttk.Label(framePinning,text = "Not Found")
pinningLabelState[4].grid(column = 3, row = 6, sticky = W)

main.resizable(width=False, height=False)
main.mainloop()
