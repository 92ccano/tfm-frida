#!/usr/bin/env python 
from Tkinter import *
import ttk
import frida
import sys
import os 
from optparse import OptionParser
import subp

main = Tk()
main.title('FriOS tool')
main.geometry('700x500')

rows = 0

while rows < 50 :
	main.rowconfigure(rows, weight = 1)
	main.columnconfigure(rows, weight = 1)
	rows = rows + 1

def startCallback():
    if __name__ == '__main__':
	    try:
		path = "hooks"
		testVar = ""
		enterTimes = 0
		parser = OptionParser(usage="usage: %prog [options] <process_to_hook>",version="%prog 1.0")
		parser.add_option("-A", "--attach", action="store_true", default=False,help="Attach to a running process")
		parser.add_option("-S", "--spawn", action="store_true", default=False,help="Spawn a new process and attach")

		os.system('python hooker_simulator.py -A Gadget')

		(options, args) = parser.parse_args()
		if (options.spawn):
			print ("[*] Spawning "+ str(args[0]))
	       		pid = frida.get_remote_device().spawn([args[0]])
			session = frida.get_remote_device().attach(pid) 
		elif (options.attach):
			print ("[*] Attaching to "+str(args[0]))
	       		session = frida.get_remote_device().attach(str(args[0]))
		else:
			print ("Error")
			print ("[X] Option not selected. View --help option.")
			sys.exit(0)

		for filename in os.listdir(path):
			name, ext = os.path.splitext(filename)
			if (ext == ".enabled"):
				print "[*] Parsing hook: "+filename
				hook = open(path+os.sep+filename, "r")
				script = session.create_script(hook.read())
				script.on('message', on_message)
				script.load()    

		while testVar != "Exit" and testVar != "Result":
			if enterTimes == 0:
				testVar = raw_input()
				enterTimes =+ 1
			else:
				testVar = raw_input("Enter ""Result"" to get the information reported until now.\nEnter ""Exit"" to exit the program: \n")

	    except KeyboardInterrupt:
	        sys.exit(0)

def on_message(message, data):



nb = ttk.Notebook(main)
nb.grid(row = 1, column = 0, columnspan = 50, rowspan = 50,sticky = 'NESW')
 
mainPage = ttk.Frame(nb)
nb.add(mainPage, text = "Complete")




b = ttk.Button(mainPage, text = "Attach & Start!", command = startCallback)
b.pack()

page1 = ttk.Frame(nb)
nb.add(page1, text = "Storage")
ttk.Label(page1,text = "NSUserDefaults: ").grid(column = 2, row = 0, sticky = W)
ttk.Label(page1,text = "CoreData: ").grid(column = 2,row = 2, sticky = W)
ttk.Label(page1,text = "RealmIO: ").grid(column = 2, row = 4, sticky = W)
ttk.Label(page1, text = "SQLite3: ").grid(column = 2, row = 6, sticky = W)
ttk.Label(page1, text = "NSFileManager: ").grid(column = 2, row = 8, sticky = W)
ttk.Label(page1, text = "Clipboard last item: ").grid(column = 2, row = 10, sticky = W)

page2 = ttk.Frame(nb)
nb.add(page2, text = "Connectivity")
ttk.Label(page2, text = "URL Instantiate: ").grid(column = 2, row = 0, sticky = W)
ttk.Label(page2, text = "NSHost: ").grid(column = 2, row = 2, sticky = W)

page3 = ttk.Frame(nb)
nb.add(page3, text = "Jailbreak Detection")
ttk.Label(page3, text = "Has access to jailbreak paths: ").grid(column = 2,row = 0, sticky = W)
ttk.Label(page3, text = "Has admin rights to forbiden paths: ").grid(column = 2, row = 2, sticky = W)
ttk.Label(page3, text = "Cydia URL Schema").grid(column = 2, row = 4, sticky = W)
'''Left catching jailbreak through C functions'''


page4 = ttk.Frame(nb)
nb.add(page4, text = "Pinning")
ttk.Label(page4, text = "SSL Handshake: ").grid(column = 2, row = 0, sticky = W)
ttk.Label(page4, text = "SSL Set session option: ").grid(column = 2, row = 2, sticky = W)
ttk.Label(page4, text = "SSL Create Context: ").grid(column = 2, row = 4, sticky = W)
ttk.Label(page4, text = "Chain Validation: ").grid(column = 2, row = 4, sticky = W)
ttk.Label(page4, text = "Cert Validation: ").grid(column = 2, row = 6, sticky = W)


main.mainloop()
